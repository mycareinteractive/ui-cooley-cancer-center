var Intro = View.extend({
    
    id: 'intro',
    
    template: 'intro.html',
    
    css: 'intro.css',
    
    className: 'languages',
    
    /**********************************************************************************
     * Overwrite functions; from base class
     *********************************************************************************/
    navigate: function(key)    {
	
	    var ignoredKeys = ['MENU', 'HOME', 'EXIT', 'BACK', 'POWR', 'CLOSE', 'CLOSEALL'];
        if(ignoredKeys.indexOf(key) >= 0) {
            return true;
        }

		var $curr = $('#intro a.active');
		var $next = $curr.next();
		if($next.length<=0) 
			$next = $curr.prev();
        if(key == 'POWR') {
            return false;
        }
		else if (key == 'ENTER') {  // default link click             								
			return this.click($curr);
		}
        
        return false;
    },
    
    click: function($jqobj) {
        var linkid = $jqobj.attr('id');
        window.settings = window.settings || {};
        window.settings.language = linkid;
        this.destroy();

        if(linkid == 'en' || linkid == 'es' || linkid == 'vi') {
		    // tracking
            nova.tracker.event('language', 'introset', linkid);
			
			// send language back to server
            setLanguage(linkid); 
            var primary = new Primary({});
            primary.render();
            return true;			
        }
        
        return false;
    },
    
    renderData: function() {
        var context = this;
        var ewf = ewfObject();     
        msg(ewf.remote);
		
        this.$('#remote').addClass(ewf.remote);
		
		
        if(ewf.remote!='') {            
            var div = '#instruction.'+ewf.remote;                 
            this.$('#instruction').hide();
			this.$('.mycare.swingarm').show()
			this.$('.mycare.tv').hide()
            this.$(div).show();            
        } else {
            this.$('#instruction.swingarm').hide();            
			this.$('.mycare.tv').show()
			this.$('.mycare.swingarm').hide()            
        }
        
        
    },
    
    /**********************************************************************************
     * Own public functions; Outside can call these functions
     *********************************************************************************/
    
    /**********************************************************************************
     * Private functions; Starts with '_' only used internally in this class
     *********************************************************************************/
});

